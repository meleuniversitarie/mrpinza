package app.akexorcist.mrpinza;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.bluetooth.BluetoothAdapter;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;
import app.akexorcist.bluetotohspp.library.BluetoothSPP.BluetoothConnectionListener;
import app.akexorcist.bluetotohspp.library.BluetoothSPP.OnDataReceivedListener;

import java.nio.ByteBuffer;

public class Main extends Activity {

    BluetoothSPP bt;

    TextView textStatus, textPinza, textOstacoli;

    Menu menu;

    byte[] bytes = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ImageButton btnUp = (ImageButton) findViewById(R.id.btnUp);
        btnUp.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(1).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(10).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        ImageButton btnLeft = (ImageButton) findViewById(R.id.btnLeft);
        btnLeft.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(3).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(30).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        ImageButton btnDown = (ImageButton) findViewById(R.id.btnDown);
        btnDown.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(2).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(20).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        ImageButton btnRight = (ImageButton) findViewById(R.id.btnRight);
        btnRight.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(4).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(40).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        ImageButton btnMore = (ImageButton) findViewById(R.id.btnMore);
        btnMore.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(6).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(60).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        ImageButton btnLess = (ImageButton) findViewById(R.id.btnLess);
        btnLess.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(7).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(70).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        ImageButton btnCustom = (ImageButton) findViewById(R.id.btnCustom);
        btnCustom.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    bytes = ByteBuffer.allocate(4).putInt(9).array();
                    bt.send(bytes, false);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    bytes = ByteBuffer.allocate(4).putInt(90).array();
                    bt.send(bytes, false);
                }
                return true;
            }
        });

        textStatus = (TextView)findViewById(R.id.textStatus);
        textOstacoli = (TextView)findViewById(R.id.textOstacoli);
        textPinza = (TextView)findViewById(R.id.textPinza);

        bt = new BluetoothSPP(this);

        if(!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext(), "Bluetooth is not available", Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setOnDataReceivedListener(new OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                String type, number;
                type = TextUtils.substring(message, 0, 1);
                number = TextUtils.substring(message, 1, message.length());
                //textRead.append(message + "\n");
                if(TextUtils.equals(type, "O")){
                    textOstacoli.setText("Ostacoli incontrati: " +number);
                }
                if(TextUtils.equals(type, "P")){
                    textPinza.setText("Pinze chiuse: " +number);
                }
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothConnectionListener() {
            public void onDeviceDisconnected() {
                textStatus.setText("Status : Not connect");
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_connection, menu);
            }

            public void onDeviceConnectionFailed() {
                textStatus.setText("Status : Connection failed");
            }

            public void onDeviceConnected(String name, String address) {
                textStatus.setText("Status : Connected to " + name);
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_disconnection, menu);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_connection, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_android_connect) {
            bt.setDeviceTarget(BluetoothState.DEVICE_ANDROID);
			/*
			if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
    			bt.disconnect();
            */
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        } else if(id == R.id.menu_device_connect) {
            bt.setDeviceTarget(BluetoothState.DEVICE_OTHER);
			/*
			if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
    			bt.disconnect();
            */
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        } else if(id == R.id.menu_disconnect) {
            if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
                bt.disconnect();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }

    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
