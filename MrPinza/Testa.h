#include "SoftwareServo.h"
#include <Arduino.h>
#include <NewPing.h>

class Testa: public SoftwareServo {
public:
    Testa(int pin,int initialAngle);
    void guarda(int pos);
    int getpin();
    void connect();
    void inizializzaPinza();
    void stringiPinza();
    void allentaPinza();
    void giraTesta(int *availableDir);
    void guardaAvanti();
private:
    int availableDir[2];
    int pin;
    int pos;
    SoftwareServo myServo;
};
