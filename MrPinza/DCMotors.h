#ifndef DCMOTORS_H
#define DCMOTORS_H

#include <Arduino.h>

class DCMotors {
public:
    DCMotors(int pin1M1, int pin2M1, int pin1M2, int pin2M2, int enableMotors,int pwm);
    bool avanti();
    bool indietro();
    bool destra();
    bool sinistra();
    bool stop();
    void connect();
private:
    int pin1M1;
    int pin1M2;
    int pin2M1;
    int pin2M2;
    int enableMotors;
    int pwm;
};

#endif
