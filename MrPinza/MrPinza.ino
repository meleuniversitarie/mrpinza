enum State {
  Avanti = 1,
  Indietro = 2,
  Sinistra = 3,
  Destra = 4,
  Clacson = 5,
  Stringi = 6,
  Allenta = 7,
  Fermo = 8,
  Automatico = 9,
};

#include "Task.h"
#include <NewPing.h>
#include "Pinza.h"
#include "PinzaTask.h"
#include "TestaTask.h"
#include "DCMotors.h"
#include "Scheduler.h"
#include "automaticoTask.h"

int dir = Fermo;


DCMotors motors(3, 4, 6, 7, 9, 100);
NewPing proxSenseM(5, 2, 100);

int pos = 105, posHead = 90;  // variable to store the servo position
int ledp = 13, leds = 8;
int speakerPin = 12;
int directions[2];
int temp[2];
bool automatico=false;

int mis = 0;

Scheduler scheduler;

void setup()
{
  motors.connect();
  Serial.begin(9600);
  pinMode(leds, OUTPUT);
  pinMode(ledp, OUTPUT);
  pinMode(speakerPin, OUTPUT);

  scheduler.init(50);

  TestaTask* t0 = new TestaTask(11, 90,leds,&proxSenseM,&motors);
  t0->init(100);
  scheduler.addTask(t0);

  PinzaTask* t1 = new PinzaTask(10, 115,ledp);
  t1->init(100);
  scheduler.addTask(t1);

  automaticoTask* aT = new automaticoTask(&proxSenseM, &motors, t0,t1);
  aT->init(100);
  scheduler.addTask(aT);

    for(int i=0; i<2;i++){
      directions[i]=1;
    }
}

void loop()
{
  scheduler.schedule();
}

void serialEvent() {
  TestaTask * t0= (TestaTask *) scheduler.getTaskAtIndex(0);
  t0->getDirections(directions);
  PinzaTask * t1= (PinzaTask *) scheduler.getTaskAtIndex(1);
  automaticoTask * aT= (automaticoTask *) scheduler.getTaskAtIndex(2);
  switch (Serial.read()) {
    case Avanti:
      if ( !t0->getHalt() ) {
        motors.avanti();
        dir=Avanti;
        t0->setAllAvailable();
      }
      break;
    case Indietro:
      motors.indietro();
      dir=Indietro;
      t0->setAllAvailable();
      break;
    case Sinistra:
      if (directions[0] == 1){
        motors.sinistra();
        dir=Sinistra;
      }
      break;
    case Destra:
      if (directions[1] == 1 ){
        motors.destra();
        dir=Destra;
      }
      break;
    case Clacson:
      tune = 1;
      break;
    case Allenta:
      t1->allenta = true;
      break;
    case Stringi:
      t1->stringi = true;
      break;
    case Fermo:
      motors.stop();
      t0->setForward(false);
      break;
    case Automatico:
      automatico=!automatico;
      motors.stop();

      aT->setAutomatico(automatico);
      break;
    case 10:
    case 20:
    case 30:
    case 40:
      motors.stop();
      break;
    case 60:
      t1->stringi = false;
      break;
    case 70:
      t1->allenta = false;
      break;
    default:
      break;
  }
}
