//
//  TestaTask.hpp
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#ifndef TestaTask_h
#define TestaTask_h

#include "Task.h"
#include "Testa.h"
#include <NewPing.h>
#include "DCMotors.h"

class TestaTask: public Task{
public:
    TestaTask(int pin, int initialAngle,int ledPin,NewPing *sensor,DCMotors *motors);
    void setForward(bool value);
    void init(int period);
    void tick();
    bool getHalt();
    void getDirections(int *arr);
    void setAllAvailable();
private:
    int ledPin;
    int pin;
    int pos;
    Testa *testa;
    NewPing * proxSense;
    DCMotors * motors;
    bool forward;
};
#endif /* TestaTask_h */
