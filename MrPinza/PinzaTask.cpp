//
//  PinzaTask.cpp
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#include "PinzaTask.h"

bool hasClosed=false;

PinzaTask::PinzaTask(int pin, int initialAngle,int ledPin){
  this->pin=pin;
  this->pos=initialAngle;
  this->ledPin=ledPin;
  chiusure = 0;
}

void PinzaTask::init(int period){
 Task::init(period);
 this->allenta=false;
  this->stringi=false;
 pinza=new Pinza(this->pin, this->pos);
 pinza->connect();
 pinza->inizializzaPinza();
}

void PinzaTask::tick(){
        SoftwareServo::refresh();

  if (stringi) {
    hasClosed=true;

    pinza->stringiPinza();
    digitalWrite(this->ledPin, HIGH);
  } else if (allenta) {

    pinza->allentaPinza();

    if (hasClosed && pinza->isOpen()){
      chiusure++;
      hasClosed=false;
      String stringOne = "P";
    String stringThree = stringOne + chiusure;
    Serial.println(stringThree);
    }
    digitalWrite(this->ledPin, HIGH);
  } else {
    digitalWrite(this->ledPin, LOW);
  }
}

int PinzaTask::getChiusure(){
  return this->chiusure;
}

void PinzaTask::chiudiPinza(){
  this->pinza->chiudiPinza();
}
