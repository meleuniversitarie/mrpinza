#import <Servo.h>
#import <Arduino.h>
#include <NewPing.h>

class MyServo {
public:
    MyServo(int pin,int initialAngle);
    void guarda(int pos);
    int getpin();
    void connect();
    void inizializzaPinza();
    void stringiPinza();
    void allentaPinza();
    void giraTesta(int *availableDir);
    void guardaAvanti();
private:

    int pin;
    int pos;
    Servo myServo;
};
