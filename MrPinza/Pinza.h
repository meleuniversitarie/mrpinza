//
//  Pinza.h
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#ifndef Pinza_h
#define Pinza_h
#include "SoftwareServo.h"
#include <Arduino.h>
#define STRINGI 6

class Pinza: public SoftwareServo {
public:
    Pinza(int pin,int initialAngle);
    int getpin();
    void connect();
    void inizializzaPinza();
    void stringiPinza();
    void allentaPinza();
    bool isOpen();
    void chiudiPinza();
private:

    int pin;
    int pos;
    SoftwareServo pinza;
};

#endif /* Pinza_h */
