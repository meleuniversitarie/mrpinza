//
//  TestaTask.cpp
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#include "TestaTask.h"
#define DISTANCE 35

int dirs[2];
bool halt;
int ostacoli=0;

TestaTask::TestaTask(int pin, int initialAngle,int ledPin,NewPing *sensor,DCMotors *motors){
  this->pin=pin;
  this->pos=initialAngle;
  this->ledPin=ledPin;
  this->proxSense= sensor;
  this->motors=motors;
}

void TestaTask::init(int period){
 Task::init(period);
 halt=false;
 this->forward=false;
 testa=new Testa(this->pin, this->pos);
 testa->connect();
 testa->guardaAvanti();
 this->setAllAvailable();


}

void TestaTask::tick(){
 int mis = proxSense->convert_cm(proxSense->ping_median());
 
  if (mis > DISTANCE || mis == 0) {
    digitalWrite(this->ledPin, LOW);
    halt=false;
    this->setForward(true);
  } else {
    digitalWrite(this->ledPin, HIGH);
    halt=true;
  }
  if ( forward && halt) {
    this->motors->stop();
    this->forward=false;
    this->testa->giraTesta(dirs);
    ostacoli++;

  String stringOne = "O";
  String stringThree = stringOne + ostacoli;
  Serial.println(stringThree);
  }


}

void TestaTask::getDirections(int *arr){

    for(int i=0; i<2;i++){
       arr[i]=dirs[i];
    }

}

void TestaTask::setAllAvailable(){
  for(int i=0; i<2;i++){
       dirs[i]=1;
    }
}

bool TestaTask::getHalt(){
  return halt;
}

void TestaTask::setForward(bool value){
   this->forward=value;
}
