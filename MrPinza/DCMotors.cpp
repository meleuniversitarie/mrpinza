#include "DCMotors.h"

DCMotors::DCMotors(int pin1M1, int pin2M1, int pin1M2, int pin2M2, int enableMotors, int pwm) {
  this->pin1M1 = pin1M1;
  this->pin1M2 = pin1M2;
  this->pin2M1 = pin2M1;
  this->pin2M2 = pin2M2;
  this->enableMotors = enableMotors;
  this->pwm = pwm;
}

void DCMotors::connect() {
  pinMode(this->pin1M1, OUTPUT);
  pinMode(this->pin2M1, OUTPUT);
  pinMode(this->pin1M2, OUTPUT);
  pinMode(this->pin2M2, OUTPUT);
  pinMode(this->enableMotors, OUTPUT);
  digitalWrite(this->enableMotors, HIGH);
}

bool DCMotors::avanti() {
  digitalWrite(this->pin1M1, this->pwm);
  digitalWrite(this->pin2M1, LOW);
  digitalWrite(this->pin1M2, this->pwm);
  digitalWrite(this->pin2M2, LOW);
  return true;
}

bool DCMotors::indietro() {
  digitalWrite(this->pin1M1, LOW);
  digitalWrite(this->pin2M1, this->pwm);
  digitalWrite(this->pin1M2, LOW);
  digitalWrite(this->pin2M2, this->pwm);
  return true;
}

bool DCMotors::destra() {
  digitalWrite(this->pin1M1, LOW);
  digitalWrite(this->pin2M1, this->pwm-50);
  digitalWrite(this->pin1M2, LOW);
  digitalWrite(this->pin2M2, LOW);
  return true;
}

bool DCMotors::sinistra() {
  digitalWrite(this->pin1M1, LOW);
  digitalWrite(this->pin2M1, LOW);
  digitalWrite(this->pin1M2, LOW);
  digitalWrite(this->pin2M2, this->pwm-50);
  return true;
}

bool DCMotors::stop() {
  digitalWrite(this->pin1M1, LOW);
  digitalWrite(this->pin2M1, LOW);
  digitalWrite(this->pin1M2, LOW);
  digitalWrite(this->pin2M2, LOW);
  return true;
}
