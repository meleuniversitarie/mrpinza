#ifndef SoftwareServo_h
#define SoftwareServo_h

#include <Arduino.h>
#include <inttypes.h>

class SoftwareServo
{
  private:
    uint8_t pin;
    uint8_t angle;
    uint16_t pulse0;
    uint8_t min16;
    uint8_t max16;
    class SoftwareServo *next;
    static SoftwareServo* first;
  public:
    SoftwareServo();
    uint8_t attach(int);
    void detach();
    void write(int);
    uint8_t read();
    uint8_t attached();
    void setMinimumPulse(uint16_t);
    void setMaximumPulse(uint16_t);
    static void refresh();
};

#endif
