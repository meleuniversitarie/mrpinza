//
//  TestaTask.cpp
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#include "automaticoTask.h"

int autoDir = 0;
int autoDirections[2];
bool autoHalt;
TestaTask *autoHeadTask;
bool trigger = false;
PinzaTask *autoPinzaTask;

automaticoTask::automaticoTask(NewPing *sensor, DCMotors *motors,TestaTask *headTask,PinzaTask *pinzaTask){
  this->proxSense= sensor;
  this->motors=motors;
  autoHeadTask=headTask;
  autoPinzaTask=pinzaTask;
}

void automaticoTask::init(int period){
 Task::init(period);
}

void automaticoTask::tick(){
    if(trigger){
    autoPinzaTask->chiudiPinza();
    autoDir=1;
    motors->avanti();
    if(autoHeadTask->getHalt()){
      this->setDirections();
      Serial.println(autoDirections[0]);
      Serial.println(autoDirections[1]);
      if(autoDirections[0]==1){
        motors->sinistra();
      }else if(autoDirections[1]==1){
        motors->destra();
      }else{
        motors->indietro();
        autoHalt=false;
      }
    }
  }
}

void automaticoTask::setDirections(){
  autoHeadTask->getDirections(autoDirections);
}

void automaticoTask::setAutomatico(bool var){
  trigger=var;
}
