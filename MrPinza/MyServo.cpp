#include "MyServo.h"

Servo myServo;
NewPing proxSense(5,2,100);
MyServo::MyServo(int pin,int initialAngle){
    this->pin = pin;
    this->pos=initialAngle;
}

void MyServo::connect(){
  this->myServo.attach(this->pin);
}

void MyServo::guarda(int pos){
    this->myServo.write(pos);
}

int MyServo::getpin(){
   return this->pin;
}

void MyServo::inizializzaPinza(){
  guarda(115);
}


void MyServo::stringiPinza(){
  this->pos=constrain (this->pos+10, 115, 160);

  guarda(this->pos);
}

void MyServo::allentaPinza(){
  this->pos=constrain (this->pos-10, 115, 160);
  guarda(this->pos);
}

void MyServo::guardaAvanti(){
  this->pos=90;
  guarda(this->pos);
}

void MyServo::giraTesta(int *availableDir){
    this->pos=10;
    guarda(this->pos);
    delay(500);
    proxSense.convert_cm(proxSense.ping_median()) > 10 ? availableDir[0]=1 : availableDir[0]=0 ;
    this->pos=170;
    guarda(this->pos);
    delay(500);
    proxSense.convert_cm(proxSense.ping_median()) > 10 ? availableDir[1]=1 : availableDir[1]=0 ;
    guardaAvanti();
}
