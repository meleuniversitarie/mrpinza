//
//  Pinza.c
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#include "Pinza.h"

Pinza::Pinza(int pin,int initialAngle){
    this->pin = pin;
    this->pos=initialAngle;
}

void Pinza::connect(){
    this->pinza.attach(this->pin);
}

int Pinza::getpin(){
    return this->pin;
}

void Pinza::inizializzaPinza(){
    this->pinza.write(this->pos);
    SoftwareServo::refresh();
}


void Pinza::stringiPinza(){
    this->pos=constrain (this->pos+10, 115, 160);
    this->pinza.write(this->pos);
}

void Pinza::allentaPinza(){
    this->pos=constrain (this->pos-10, 115, 160);
    this->pinza.write(this->pos);
}

bool Pinza::isOpen(){
  return this->pos<=115 ? true : false;
}

void Pinza::chiudiPinza(){
    this->pos=160;
    this->pinza.write(this->pos);
}
