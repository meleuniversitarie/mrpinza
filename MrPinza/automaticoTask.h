//
//  automaticoTask.hpp
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#ifndef automaticoTask_h
#define automaticoTask_h
#include "Task.h"
#include "TestaTask.h"
#include "PinzaTask.h"
#include "Pinza.h"
#include <NewPing.h>
#include "DCMotors.h"

class automaticoTask: public Task{
public:
    automaticoTask(NewPing *sensor,DCMotors *motors,TestaTask *headTask, PinzaTask *pinzaTask);
    void init(int period);
    void tick();
    void setDirections();
    void setAutomatico(bool var);
private:

    NewPing * proxSense;
    DCMotors * motors;

};
#endif
