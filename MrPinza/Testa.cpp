#include "Testa.h"

SoftwareServo myServo;
NewPing proxSense(5,2,100);
Testa::Testa(int pin,int initialAngle){
    this->pin = pin;
    this->pos=initialAngle;
    for(int i=0; i<2;i++){
       this->availableDir[i]=0;
    }
}

void Testa::connect(){
  this->myServo.attach(this->pin);
}

void Testa::guarda(int pos){
    this->myServo.write(pos);
}

int Testa::getpin(){
   return this->pin;
}

void Testa::guardaAvanti(){
  this->pos=90;
  guarda(this->pos);
}

void Testa::giraTesta(int *availableDir){
    this->pos=10;
    guarda(this->pos);
    for (int i=0; i<40 ; i++){
        SoftwareServo::refresh();
        delay(20);
    }
    proxSense.convert_cm(proxSense.ping_median()) > 10 ? availableDir[0]=1 : availableDir[0]=0 ;
    this->pos=170;
    guarda(this->pos);
for (int i=0; i<40 ; i++){
        SoftwareServo::refresh();
        delay(20);
    }
    proxSense.convert_cm(proxSense.ping_median()) > 10 ? availableDir[1]=1 : availableDir[1]=0 ;
    guardaAvanti();
      for (int i=0; i<40 ; i++){
        SoftwareServo::refresh();
        delay(20);
    }
}
