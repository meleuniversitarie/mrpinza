//
//  PinzaTask.hpp
//
//
//  Created by Marco Marinoni on 23/01/16.
//
//

#ifndef PinzaTask_h
#define PinzaTask_h


#include "Task.h"
#include "Pinza.h"

class PinzaTask: public Task{
public:
    PinzaTask(int pin, int initialAngle,int ledPin);
    void init(int period);
    void tick();
    bool stringi;
    bool allenta;
    int getChiusure();
    void chiudiPinza();
private:
    int ledPin;
    int pin;
    int pos;
    int chiusure;
    Pinza *pinza;
};
#endif /* PinzaTask_h */
