import processing.serial.*;

Serial port;

//Cambiare la porta in modo che corrisponda a quella che "occupa" arduino

//String portname = "/dev/cu.usbmodemFA131";
//String portname = "/dev/cu.usbmodemFD121";
String portname = "/dev/cu.HC-06-DevB";
int baudrate = 9600;
int value = 0;  
int lastKey=9;
void setup(){
 port = new Serial(this, portname, baudrate);
 println(port);                  
}

void draw(){
  background(0);
 } 

//I comandi sono:
//w per andare avanti
//a per andare a sinistra
//d per andare a destra
//s per andare indietro
//q per usare il clacson
//p per avviare la musica
//+ o = o ^ o ì per fare chiudere la pinza (i caratteri speciali sono dovuti dalla tastiera americana)
//- o \ per fare aprire la pinza 
void keyPressed(){
  
   switch (key){
   case 'w' :
   if (lastKey==1)
     break;
     port.write (1);
     lastKey=1;
     break;
   case 'a' :
   if (lastKey==3)
     break;
     port.write (3);
     lastKey=3;
     break;
   case 'd' :
   if (lastKey==4)
     break;
     port.write (4);
     lastKey=4;
     break;
   case 's' :
   if (lastKey==2)
     break;
     port.write (2);
     lastKey=2;
     break;
 
   case 'q' :
     port.write (5);
     break;
     
   case 'p' :
     port.write (9);
     break;
     
   case '+' :
   case '^' :
   case '=' :
   case 'ì' :
   if (lastKey==6)
     break;
     port.write (6);
     lastKey=6;
     break;
     
   case '-' :
   case '\'' :
   if (lastKey==7)
     break;
     port.write (7);
     lastKey=7;
     break;
   
   }
}

void keyReleased(){
  lastKey=10;
  
  println(key);
  
  switch (key){
    case 'w' :
     port.write (10);
     break;
     
   case 'a' :
     port.write (30);
     break;
   
   case 's' :
     port.write (20);
     break;
   
   case 'd' :
     port.write (40);
     break; 
   
   case '+' :
   case '^' :
   case '=' :
   case 'ì' :
     port.write (60);
     break;
   case '-' :
   case '\'' :
     port.write (70);
     break;  
  }
}
